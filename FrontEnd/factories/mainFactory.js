main_module.factory('mainFactory', function ($resource, $http, $location) {
    
    var path, resoursePath, i;
    
    var factory = {};
    
    factory.setFactoryPaths = function () {
        
        //Check current path and set resoursePath, to be used later on this controller
        path = location.href.split("#/")[1];
        if (path === "horse" || path === "modifyHorse" || path === "addHorse") {
            resoursePath = "/horses";
        } else if (path === "feedHorse" || path === "modifyFeedHorse" || path === "addFeedHorse") {
            resoursePath = "/feeds";
        } else if (path === "healthHorse" || path === "modifyHealthHorse" || path === "addHealthHorse") {
            resoursePath = "/healths";
        } else if (path === "horseWelfare" || path === "modifyHorseWelfare" || path === "addHorseWelfare") {
            resoursePath = "/welfares";
        } else if (path === "horseEquipment" || path === "modifyHorseEquipment" || path === "addHorseEquipment") {
            resoursePath = "/equipments";
        } else if (path === "horsePasturage" || path === "modifyHorsePasturage" || path === "addHorsePasturage") {
            resoursePath = "/pasturages";
        } else if (path === "horsePasturageGuest" || path === "modifyHorsePasturageGuest" || path === "addHorsePasturageGuest") {
            resoursePath = "/pasturageguests";
        } else if (path === "person" || path === "modifyPerson" || path === "addPerson" ) {
            resoursePath = "/users";
        } else if (path === "contact" || path === "modifyContact" || path === "addContact" ) {
            resoursePath = "/contacts";
        } else if (path === "todo" || path === "modifyTodo" || path === "addTodo") {
            resoursePath = "/todos";
        } else if (path === "stableCalendar" || path === "modifyStableCalendar" || path === "addStableCalendar") {
            resoursePath = "/stablecalendars";
        } else if (path === "paddoc" || path === "modifyPaddoc" || path === "addPaddoc") {
            resoursePath = "/paddocs";
        } else if (path === "camp" || path === "modifyCamp" || path === "addCamp" || path === "campGoogle") {
            resoursePath = "/camps";
         } else if (path === "campPerson" || path === "modifyCampPerson" || path === "addCampPerson") {
            resoursePath = "/camppersons";
        } else if (path === "horseHour" || path === "modifyHorseHour" || path === "addHorseHour") {
            resoursePath = "/horsehours";
        } else if (path === "personHour" || path === "modifyPersonHour" || path === "addPersonHour" || path === "personHourList") {
            resoursePath = "/personhours";
        } else if (path === "test" || path === "modifyTest" || path === "addTest") {
            resoursePath = "/tests";
        } else if (path === "main" || path === "admin") {
            resoursePath = "/tests";
        } else {
            console.log("HTML FILE MISSING!");
        }
        
        //Initialize collectionarray
        //Do not clear it if modify* functions ongoing and if already initialized
        if (/modify/i.test(path)) {
            if (factory.collectionArray === undefined) {
                factory.selected_id = null;
                factory.collectionArray = [];
                $location.path(resoursePath);
            }
        } else {
            factory.selected_id = null;
            factory.collectionArray = [];
        }
    };
    
    //Get all collection data
    factory.getCollectionData = function (callbackFunc, searchtime) {
        $http.defaults.headers.common['x-access-token'] = sessionStorage['token'];
        if (factory.collectionArray.length === 0) {
            var resource = $resource(resoursePath, {}, {'get': {method: 'GET'}});
            resource.query(searchtime).$promise.then(function (data) {
                factory.collectionArray = data;
                callbackFunc(factory.collectionArray);
                
            }, function (error) {
                factory.collectionArray = [];
                callbackFunc(factory.collectionArray);
            });
        } else {
            
            callbackFunc(factory.collectionArray);
        }
    };
    
    //Get specific amount of collection data documents, criteria given on amount parameter
    factory.getCollectionDataAmount = function (callbackFunc, amount) {
        $http.defaults.headers.common['x-access-token'] = sessionStorage['token'];
        var resource = $resource(resoursePath, {}, {'get': {method: 'GET'}});
        resource.query(amount).$promise.then(function (data) {
            factory.collectionArray = data;
            callbackFunc(factory.collectionArray);

        }, function (error) {
            factory.collectionArray = [];
            callbackFunc(factory.collectionArray);
        });
        
    };
    
    //Get specific time interval of collection data documents, interval criteria given on time parameter
    factory.getCollectionDataAll = function (callbackFunc, time) {
        $http.defaults.headers.common['x-access-token'] = sessionStorage['token'];
        var resource = $resource(resoursePath + '/all', {}, {'get': {method: 'GET'}});
        resource.query(time).$promise.then(function (data) {
            factory.collectionArray = data;
            callbackFunc(factory.collectionArray);

        }, function (error) {
            factory.collectionArray = [];
            callbackFunc(factory.collectionArray);
        });
        
    };
    
    //Search selected item, search item criteria given on item parameter
    factory.searchItem = function (callbackFunc, item) {
        $http.defaults.headers.common['x-access-token'] = sessionStorage['token'];
        var resource = $resource(resoursePath + '/search', {keyword: item}, {'get': {method: 'GET'}});
        resource.query(item).$promise.then(function (data) {
            factory.collectionArray = data;
            callbackFunc(factory.collectionArray);
                
        }, function (error) {
            factory.collectionArray = [];
            callbackFunc(factory.collectionArray);
        });
    };
    
    //Send email
    factory.sendEmail = function (data) {
        $http.defaults.headers.common['x-access-token'] = sessionStorage['token'];
        var resource = $resource(resoursePath + '/sendEmail', {}, {'post': {method: 'POST'}});
        return resource.post(data).$promise
    };
    
    //Insert new data to db
    factory.insertCollectionData = function (data) {
        $http.defaults.headers.common['x-access-token'] = sessionStorage['token'];
        var resource = $resource(resoursePath, {}, {'post': {method: 'POST'}});
        return resource.post(data).$promise;
    };
    
    //Updates the data to back end
    factory.updateData = function (data) {
        $http.defaults.headers.common['x-access-token'] = sessionStorage['token'];
        var resource = $resource(resoursePath, {}, {'put': {method: 'PUT'}});
        return resource.put(data).$promise;
    };
    
    //Delete the data from db
    factory.deleteData = function (data) {
        $http.defaults.headers.common['x-access-token'] = sessionStorage['token'];
        $http.defaults.headers.common['content-type'] = 'application/json';
        var resource = $resource(resoursePath, {}, {'delete': {method: 'DELETE'}});
        return resource.delete(data).$promise;
    };
    
    //Store the main page text to db
    factory.writeText = function (data) {
        $http.defaults.headers.common['x-access-token'] = sessionStorage['token'];
        var resource = $resource("/texts/writetext", {}, {'put': {method: 'PUT'}});
        return resource.put(data).$promise;
    };
    
    //Store email settings to db
    factory.writeEmailAddress = function (data) {
        $http.defaults.headers.common['x-access-token'] = sessionStorage['token'];
        var resource = $resource("/texts/writeEmailAddress", {}, {'put': {method: 'PUT'}});
        return resource.put(data).$promise;
    };
    
    /**
      *This function searches a document from array containing an id
      *that was stored when user cliked the row in the partial_dataView
      *page. When it finds the correct one from the array, it returns
      *that object.
      */
    factory.getSelectedDocument = function () {
        for (i = 0; i < factory.collectionArray.length; i++) {
            
            if (factory.collectionArray[i]._id === factory.selected_id) {
                return factory.collectionArray[i];
            }
        }
    };
    
    //Factory must always return an object!!!!
    return factory;
    
});