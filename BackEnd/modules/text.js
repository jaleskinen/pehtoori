"use strict";
var express = require("express");
var db = require('./queries');

var router = express.Router();

//Handle get request for /readtext context
router.get('/readtext', function (req, res) {
    db.readText(req, res);
});

//Handle put request for /writetext context
router.put('/writetext', function (req, res) {
    db.writeText(req, res);
});

//Handle get request for /readEmailAddress context
router.get('/readEmailAddress', function (req, res) {
    db.readEmailAddress(req, res);
});

//Handle put request for /writeEmailAddress context
router.put('/writeEmailAddress', function (req, res) {
    db.writeEmailAddress(req, res);
});

module.exports = router;