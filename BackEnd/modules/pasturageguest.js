"use strict";
var express = require("express");
var db = require('./queries');

var router = express.Router();

//Handle GET request for /pasturageguest context
router.get('/', function (req, res) {
    
    db.getAllPasturageGuests(req, res);
});

//Handle POST request for /pasturageguest context
router.post('/', function (req, res) {
    
    db.saveNewPasturageGuest(req, res);
});

//Handle UPDATE request for /pasturageguest context
router.put('/', function (req, res) {
    
    db.updatePasturageGuest(req, res);
});

//Handle DELETE request for /pasturpasturageguestage context
router.delete('/', function (req, res) {
    
    db.deletePasturageGuest(req, res);
});

module.exports = router;