//var Person = require('./database').Person;
var db = require('./database');

var LocalStrategy = require('passport-local').Strategy;

module.exports = function(passport) {
    //console.log('function(passport)');
    passport.serializeUser(function(username, done) {
        done(null, username._id);
    });

    // used to deserialize the user
    passport.deserializeUser(function(id, done) {
        db.Person.findById(id, function(err, username) {
            done(err, username);
        });
    });
    
    passport.use('local-login', new LocalStrategy({
        usernameField : 'username',
        passwordField : 'password',
        passReqToCallback : true 
    },function(req,username,password,done){
        //console.log('local-login: ' + password);

        db.Person.findOne({username:username},function(err,data){
            //Some error occured
            if(err){
                //console.log('local-login err');
                return done(err);
            }
            //No such user
            if(!data){
                //console.log('local-login !data');
                return done(null, false, "failed to get user info");
            }
            //Incorrect password
            if(!data.validPassword(password)){
                //console.log('local-login validPassword');
                return done(null, false, "wrong password"); 
            }
            // All fine continue
            return done(null, data);
        });
        
    }));
}

