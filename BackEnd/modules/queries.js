/*jslint nomen: true */
"use strict";
var db = require('./database');
var Person = require('./database').Person;
var jwt = require('jsonwebtoken');
var server = require('../../server');
var nodemailer = require('nodemailer');

/* This function gets all person ducuments from person collection */
exports.getAllPersons = function (req, res) {
    
    db.Person.find(function (err, data) {
        
        if (err) {
            res.status(500).send("Error in database!!");
        } else {
            res.status(200).send(data);
        }
    });
};

/* This function saves new person information to person collection */
exports.saveNewPerson = function (req, res) {

    var user = new db.Person(req.body);
    user.save(function (err) {
        
        if (err) {
            res.status(500).send({status: err.message});
        } else {
            res.status(200).send({status: "Ok"});
        }
    });
};

/* This function deletes one person from person collection */
exports.deletePerson = function (req, res) {
    
    db.Person.findOne({_id:req.query.id}, function(err,data){
        
        //Remove person working hours before removing person
        db.Personhour.remove({_id:{$in:data.personhours} }, function(err,removed){

            if (err) {
                res.status(500).send({messsage:err.message});
            } else {
                
                //Remove person only if personhours remove success
                db.Person.remove({_id: req.query.id}, function (err, data) {

                    if (err) {
                        res.status(500).send({message:err.message});
                    } else {

                        if(err){;
                            res.status(500).send({messsage:err.message});
                        }else{
                            res.status(200).send({message:'Delete success'});
                        }
                    }
                });
            }
        });
    });
};

/* This method updates person information to person collection */
exports.updatePerson = function (req, res) {

    var updateData = {
        Nimi: req.body.Nimi,
        Osoite: req.body.Osoite,
        userclass: req.body.userclass,
        Puhelin: req.body.Puhelin,
        Sahkoposti: req.body.Sahkoposti,
        Internet: req.body.Internet,
        Tyosuhde: req.body.Tyosuhde
    };
    db.Person.update({_id: req.body.id}, updateData, function (err) {
        if (err) {
            res.status(500).json({message: err.message});
        } else {
            res.status(200).json({message: "Data updated"});
        }
    });
};

/* This function gets all ducuments from Contact collection */
exports.getAllContacts = function (req, res) {
    db.Contact.find(function (err, data) {
        
        if (err) {
            res.status(500).send("Error in database!!");
        } else {
            res.status(200).send(data);
        }
    });
};

/* This function saves new Contact information to Contact collection */

exports.saveNewContact = function (req, res) {
    var contactTemp = new db.Contact(req.body);
    
    //Save it to database
    contactTemp.save(function (err, ok) {
        if (err) {
            res.status(500).send("Error in database!!");
        } else {
            res.status(200).send("Save New Contact OK!!");
        }
    });
};

/* This function deletes one Contact from Contact collection */
exports.deleteContact = function (req, res) {
    
    db.Contact.remove({_id: req.query.id}, function (err, data) {
        
        if (err) {
            res.status(500).send({message:err.message});
        } else {
            
            if(err){
                res.status(500).send({messsage:err.message});
            } else {
                res.status(200).send({message:'Delete success'});
            }
        }
    });
};

/* This method updates Contact information to Contact collection */
exports.updateContact = function (req, res) {

    var updateData = {
        Kategoria: req.body.Kategoria,
        Nimi: req.body.Nimi,
        Osoite: req.body.Osoite,
        Puhelin: req.body.Puhelin,
        Sahkoposti: req.body.Sahkoposti,
        Internet: req.body.Internet
    };
    db.Contact.update({_id: req.body.id}, updateData, function (err) {
        if (err) {
            res.status(500).json({message: err.message});
        } else {
            res.status(200).json({message: "Data updated"});
        }
    });
};

/* This function gets all ducuments from horse collection */
exports.getAllHorses = function (req, res) {
    db.Horse.find(function (err, data) {
        
        if (err) {
            res.status(500).send("Error in database!!");
        } else {
            res.status(200).send(data);
        }
    });
};

/* This function saves new Horse information to Horse collection */
exports.saveNewHorse = function (req, res) {
    
    var horseTemp = new db.Horse(req.body);
    //Save it to database
    horseTemp.save(function (err, ok) {
        
        if (err) {
            res.status(500).send("Error in database!!");
        } else {
            res.status(200).send("Save New Horse OK!!");
        }
    });
};

/* This function deletes one horse from Horse collection */
exports.deleteHorse = function (req, res) {
    
    db.Horse.findOne({_id:req.query.id}, function(err,data){
        
        //Remove horse working hours before removing person
        db.Horsehour.remove({_id:{$in:data.horsehours} }, function(err,removed){

            if (err) {
                res.status(500).send({messsage:err.message});
            } else {
                
                //Remove horse only if horsehours remove success
                db.Horse.remove({_id: req.query.id}, function (err, data) {

                    if (err) {
                        res.status(500).send({message:err.message});
                    } else {

                        if(err){;
                            res.status(500).send({messsage:err.message});
                        }else{
                            res.status(200).send({message:'Delete success'});
                        }
                    }
                });
            }
        });
    });
};

/* This method updates horse information to horse collection */
exports.updateHorse = function (req, res) {
    var updateData = {
        Nimi: req.body.Nimi,
        Kutsumanimi: req.body.Kutsumanimi,
        Syntymaaika: req.body.Syntymaaika,
        Sukuposti: req.body.Sukuposti
    };
    
    db.Horse.update({_id: req.body.id}, updateData, function (err) {
        if (err) {
            res.status(500).send(err.message);
        } else {
            res.status(200).send("Updated");
        }
    });
};

/* This function gets all ducuments from Todo collection */
exports.getAllTodos = function (req, res) {
    
    db.Todo.find(function (err, data) {
        
        if (err) {
            res.status(500).send("Error in database!!");
        } else {
            res.status(200).send(data);
        }
    });
};

/* This function saves new todo information to Todo collection */
exports.saveNewTodo = function (req, res) {
    
    var saveTemp = new db.Todo(req.body);
    //Save it to database
    saveTemp.save(function (err, ok) {
        
        if (err) {
            res.status(500).send("Error in database!!");
        } else {
            res.status(200).send("Save New Task OK!!");
        }
    });
};

/* This function deletes one todo from Todo collection */
exports.deleteTodo = function (req, res) {
    
    db.Todo.remove({_id: req.query.id}, function (err) {
        if (err) {
            res.status(500).send(err.message);
        } else {
            res.status(200).send("Delete done");
        }
    });
};

/* This method updates Todo information to Todo collection */
exports.updateTodo = function (req, res) {

    var updateData = {
        Aika: req.body.Aika,
        Ilmoittaja: req.body.Ilmoittaja,
        Ilmoitus: req.body.Ilmoitus
    };
    
    db.Todo.update({_id: req.body.id}, updateData, function (err) {
        if (err) {
            res.status(500).send(err.message);
        } else {
            res.status(200).send("Updated");
        }
    });
};
 
/* This function gets all ducuments from Stablecalendar collection */
exports.getAllStablecalendars = function (req, res) {
    
    db.Stablecalendar.find(function (err, data) {
        
        if (err) {
            res.status(500).send("Error in database!!");
        } else {
            res.status(200).send(data);
        }
    });
};

/* This function saves new document information to Stablecalendar collection */
exports.saveNewStablecalendar = function (req, res) {
    
    var saveTemp = new db.Stablecalendar(req.body);
    //Save it to database
    saveTemp.save(function (err, ok) {
        
        if (err) {
            res.status(500).send("Error in database!!");
        } else {
            res.status(200).send("Save New Task OK!!");
        }
    });
};

/* This function deletes one document from Stablecalendar collection */
exports.deleteStablecalendar = function (req, res) {
    
    db.Stablecalendar.remove({_id: req.query.id}, function (err) {
        if (err) {
            res.status(500).send(err.message);
        } else {
            res.status(200).send("Delete done");
        }
    });
};

/* This method updates information to Stablecalendar collection */
exports.updateStablecalendar = function (req, res) {
    var updateData = {
        Kellonaika: req.body.Kellonaika,
        Maanantai: req.body.Maanantai,
        Tiistai: req.body.Tiistai,
        Keskiviikko: req.body.Keskiviikko,
        Torstai: req.body.Torstai,
        Perjantai: req.body.Perjantai,
        Lauantai: req.body.Lauantai,
        Sunnuntai: req.body.Sunnuntai
    };
    
    db.Stablecalendar.update({_id: req.body.id}, updateData, function (err) {
        if (err) {
            res.status(500).send(err.message);
        } else {
            res.status(200).send("Updated");
        }
    });
}; 

/* This function gets all ducuments from Paddoc collection */
exports.getAllPaddocs = function (req, res) {
    
    db.Paddoc.find(function (err, data) {
        
        if (err) {
            res.status(500).send("Error in database!!");
        } else {
            res.status(200).send(data);
        }
    });
};

/* This function saves new document information to Paddoc collection */
exports.saveNewPaddoc = function (req, res) {
    
    var saveTemp = new db.Paddoc(req.body);
    //Save it to database
    saveTemp.save(function (err, ok) {
        
        if (err) {
            res.status(500).send("Error in database!!");
        } else {
            res.status(200).send("Save New Task OK!!");
        }
    });
};

/* This function deletes one document from Paddoc collection */
exports.deletePaddoc = function (req, res) {
    
    db.Paddoc.remove({_id: req.query.id}, function (err) {
        if (err) {
            res.status(500).send(err.message);
        } else {
            res.status(200).send("Delete done");
        }
    });
};

/* This method updates information to Paddoc collection */
exports.updatePaddoc = function (req, res) {
    var updateData = {
        Hevonen: req.body.Hevonen,
        Tarha: req.body.Tarha,
        Lauma: req.body.Lauma,
        Loimi: req.body.Loimi,
        Huomio: req.body.Huomio
    };
    
    db.Paddoc.update({_id: req.body.id}, updateData, function (err) {
        if (err) {
            res.status(500).send(err.message);
        } else {
            res.status(200).send("Updated");
        }
    });
}; 

/* This function gets all documents between startTime-endTime (if defined) from Horsehour collection */
exports.getAllHorsehours = function (req, res) {
    var startOfFetch;
    var endOfFetch;
    if (req.query.startTime === "" || req.query.startTime === undefined || req.query.startTime === null) {
        startOfFetch = "2000.00.00";
        } else {
        startOfFetch = req.query.startTime;
        }
    if (req.query.endTime === "" || req.query.endTime === undefined || req.query.endTime === null) {
        endOfFetch = "2100.00.00";
        } else {
        endOfFetch = req.query.endTime;
        }
    
    var startOfFetchJoin = startOfFetch.split('.').join("");
    var endOfFetchJoin = endOfFetch.split('.').join("");
    
    db.Horsehour.find({Timestamp: { $gte: startOfFetchJoin,  $lte: endOfFetchJoin}}).exec(function(err,data){
        
        if (err) {
            res.status(500).send("Error in database!!");
        } else {
            res.status(200).send(data);
        }
    });
};

/* This function saves new document to Horsehour collection */
exports.saveNewHorsehour = function (req, res) {
    
    var saveTemp = new db.Horsehour(req.body);
    
    //Save it to database
    saveTemp.save(function (err, ok) {
        
        db.Horse.update({Nimi:req.body.Hevonen},
                          {$push:{'horsehours':saveTemp._id}},
                          function(err,model){
        
            if (err) {
                res.status(500).send("Error in database!!");
            } else {
                res.status(200).send("Save New Item OK!!");
            }
        });
    });
};

/* This function deletes one document from Horsehour collection */
exports.deleteHorsehour = function (req, res) {
    
    db.Horsehour.remove({_id: req.query.id}, function (err) {
        if (err) {
            res.status(500).send(err.message);
        } else {
            db.Horse.update({Nimi: req.query.Hevonen}, {$pull: {'horsehours': req.query.id}}, function (err, data) {
                if(err){
                    res.status(500).send({messsage:err.message});
                }else{
                    res.status(200).send("Delete done");
                }
            });
        }
    });
};

/* This method updates document information to Horsehour collection */
exports.updateHorsehour = function (req, res) {
    
    var updateData = {
        Aika: req.body.Aika,
        Hevonen: req.body.Hevonen,
        Laji: req.body.Laji,
        Tunnit: req.body.Tunnit,
        Huomio: req.body.Huomio,
        Timestamp: req.body.Timestamp
    };
    
    db.Horsehour.update({_id: req.body.id}, updateData, function (err) {
        
        if (err) {   
            res.status(500).send(err.message);
        } else {
            res.status(200).send("Updated");
        }
    });
};

/*This function searches Horsehour database by name or by begin letters of name */
/*Also search startTime and endTime may be defined*/
exports.findHorseHours = function(req,res){
    var startOfFetch;
    var endOfFetch;
    if (req.query.startTime === "" || req.query.startTime === undefined || req.query.startTime === null) {
        startOfFetch = "2000.00.00";
        } else {
        startOfFetch = req.query.startTime;
        }
    if (req.query.endTime === "" || req.query.endTime === undefined || req.query.endTime === null) {
        endOfFetch = "2100.00.00";
        } else {
        endOfFetch = req.query.endTime;
        }
    
    var startOfFetchJoin = startOfFetch.split('.').join("");
    var endOfFetchJoin = endOfFetch.split('.').join("");
    
    db.Horsehour.find({ $and: [ {Hevonen: req.query.item}, {Timestamp: { $gte: startOfFetchJoin,  $lte: endOfFetchJoin}}]}).exec(function(err,horse){
        if (err) {
            res.status(500).send("Error in database!!");
        } else {
            res.status(200).send(horse);
        }
    });
}

/* This function gets all documents (one user) between startTime-endTime (if defined) from Personhour collection */
exports.getUserPersonhours = function (req, res) {
    var startOfFetch;
    var endOfFetch;
    var fetchName;
    if (req.query.startTime === "" || req.query.startTime === undefined || req.query.startTime === null) {
        startOfFetch = "2000.00.00";
        } else {
        startOfFetch = req.query.startTime;
        }
    if (req.query.endTime === "" || req.query.endTime === undefined || req.query.endTime === null) {
        endOfFetch = "2100.00.00";
        } else {
        endOfFetch = req.query.endTime;
        }
    if (req.query.name === "" || req.query.name === undefined || req.query.name === null) {
        fetchName = req.session.name;
        } else {
        fetchName = req.query.name;
        }

    var startOfFetchJoin = startOfFetch.split('.').join("");
    var endOfFetchJoin = endOfFetch.split('.').join("");

    db.Person.findOne({Nimi:fetchName}).populate({path: 'personhours', match: { Timestamp: { $gte: startOfFetchJoin,  $lte: endOfFetchJoin}}}).exec(function(err,data){
        
        if(data){
                res.status(200).send(data.personhours);
            } else {
                res.status(500).send("Error in search!!");
            }
    });
};

/* This function gets all documents (all users) between startTime-endTime (if defined) from Personhour collection */
exports.getAllPersonhours = function (req, res) {
    var startOfFetch;
    var endOfFetch;
    if (req.query.startTime === "" || req.query.startTime === undefined || req.query.startTime === null) {
        startOfFetch = "2000.00.00";
        } else {
        startOfFetch = req.query.startTime;
        }
    if (req.query.endTime === "" || req.query.endTime === undefined || req.query.endTime === null) {
        endOfFetch = "2100.00.00";
        } else {
        endOfFetch = req.query.endTime;
        }
    var startOfFetchJoin = startOfFetch.split('.').join("");
    var endOfFetchJoin = endOfFetch.split('.').join("");
    
    db.Personhour.find({Timestamp: { $gte: startOfFetchJoin,  $lte: endOfFetchJoin}}).exec(function (err, data) {
        
        if (err) {
            res.status(500).send("Error in database!!");
        } else {
            res.status(200).send(data);
        }
    });
};

/* This function saves new document to Personhour collection */
exports.saveNewPersonhour = function (req, res) {
    
    var saveTemp = new db.Personhour(req.body);
    
    //Save it to database
    saveTemp.save(function (err, newData) {

        db.Person.update({username:req.session.username},
                          {$push:{'personhours':saveTemp._id}},
                          function(err,model){
        
            if(err) {
                res.status(500).json({message: 'Fail'});
            } else {
                res.status(200).json({data: newData});
            }
        });
    });
};

/* This function deletes one document from Personhour collection */
exports.deletePersonhour = function (req, res) {
    
    db.Personhour.remove({_id: req.query.id}, function (err, data) {
        
        if (err) {
            res.status(500).send({message:err.message});
        } else {
            db.Person.update({username: req.session.username}, {$pull: {'personhours': req.query.id}}, function (err, data) {
                if(err){
                    res.status(500).send({messsage:err.message});
                } else {
                    res.status(200).send({message:'Delete success'});
                }
            });
        }
    });
};

/* This method updates document information to Personhour collection */
exports.updatePersonhour = function (req, res) {
    
    var updateData = {
        Paiva: req.body.Paiva,
        Ilmoittaja: req.body.Ilmoittaja,
        Tehtava: req.body.Tehtava,
        Tunnit: req.body.Tunnit,
        Timestamp: req.body.Timestamp
    };
    
    db.Personhour.update({_id: req.body.id}, updateData, function (err) {
        
        if (err) {
            res.status(500).send(err.message);
        } else {
            res.status(200).send("Updated");
        }
    });
};

/*This function searches Horsehour database by name or by begin letters of name */
/*Also search startTime and endTime may be defined*/
exports.findPersonHours = function(req,res){
    var startOfFetch;
    var endOfFetch;
    if (req.query.startTime === "" || req.query.startTime === undefined || req.query.startTime === null) {
        startOfFetch = "2000.00.00";
        } else {
        startOfFetch = req.query.startTime;
        }
    if (req.query.endTime === "" || req.query.endTime === undefined || req.query.endTime === null) {
        endOfFetch = "2100.00.00";
        } else {
        endOfFetch = req.query.endTime;
        }
    
    var startOfFetchJoin = startOfFetch.split('.').join("");
    var endOfFetchJoin = endOfFetch.split('.').join("");
    
    db.Personhour.find({ $and: [ {Ilmoittaja: req.query.item}, {Timestamp: { $gte: startOfFetchJoin,  $lte: endOfFetchJoin}}]}).exec(function(err,person){
        if (err) {
            res.status(500).send("Error in database!!");
        } else {
            res.status(200).send(person);
        }
    });
}

/* This function gets all documents from Health collection */
exports.getAllHealths = function (req, res) {
    
    db.Health.find(function (err, data) {
        
        if (err) {
            res.status(500).send("Error in database!!");
        } else {
            res.status(200).send(data);
        }
    });
};

/* This function saves new Health information to Health collection */
exports.saveNewHealth = function (req, res) {
    
    var saveTemp = new db.Health(req.body);
    
    //Save it to database
    saveTemp.save(function (err, ok) {
        
        if (err) {
            res.status(500).send("Error in database!!");
        } else {
            res.status(200).send("Save New Item OK!!");
        }
    });
};

/* This function deletes one field from Health collection */
exports.deleteHealth = function (req, res) {

    db.Health.remove({_id: req.query.id}, function (err) {
        if (err) {
            res.status(500).send(err.message);
        } else {
            res.status(200).send("Delete done");
        }
    });
};

/* This method updates Health information to Health collection */
exports.updateHealth = function (req, res) {
    
    var updateData = {
        Hevonen: req.body.Hevonen,
        Rokotukset: req.body.Rokotukset,
        Madotukset: req.body.Madotukset,
        Raspaus: req.body.Raspaus,
        Laakitys: req.body.Laakitys,
        Yleista: req.body.Yleista
    };
    
    db.Health.update({_id: req.body.id}, updateData, function (err) {
        
        if (err) {   
            res.status(500).send(err.message);
        } else {
            res.status(200).send("Updated");
        }
    });
};

/* This function gets all documents from Welfare collection */
exports.getAllWelfares = function (req, res) {
    
    db.Welfare.find(function (err, data) {
        
        if (err) {
            res.status(500).send("Error in database!!");
        } else {
            res.status(200).send(data);
        }
    });
};

/* This function saves new Welfare information to Welfare collection */
exports.saveNewWelfare = function (req, res) {
    
    var saveTemp = new db.Welfare(req.body);
    
    //Save it to database
    saveTemp.save(function (err, ok) {
        
        if (err) {
            res.status(500).send("Error in database!!");
        } else {
            res.status(200).send("Save New Item OK!!");
        }
    });
};

/* This function deletes one field from Welfare collection */
exports.deleteWelfare = function (req, res) {

    db.Welfare.remove({_id: req.query.id}, function (err) {
        if (err) {
            res.status(500).send(err.message);
        } else {
            res.status(200).send("Delete done");
        }
    });
};

/* This method updates Welfare information to Welfare collection */
exports.updateWelfare = function (req, res) {
    
    var updateData = {
        Hevonen: req.body.Hevonen,
        Hieronta: req.body.Hieronta,
        Kengitys: req.body.Kengitys,
        Yleista: req.body.Yleista
    };
    
    db.Welfare.update({_id: req.body.id}, updateData, function (err) {
        
        if (err) {   
            res.status(500).send(err.message);
        } else {
            res.status(200).send("Updated");
        }
    });
};

/* This function gets all documents from Equipment collection */
exports.getAllEquipments = function (req, res) {
    
    db.Equipment.find(function (err, data) {
        
        if (err) {
            res.status(500).send("Error in database!!");
        } else {
            res.status(200).send(data);
        }
    });
};

/* This function saves new Equipment information to Equipment collection */
exports.saveNewEquipment = function (req, res) {
    
    var saveTemp = new db.Equipment(req.body);
    
    //Save it to database
    saveTemp.save(function (err, ok) {
        
        if (err) {
            res.status(500).send("Error in database!!");
        } else {
            res.status(200).send("Save New Item OK!!");
        }
    });
};

/* This function deletes one field from Equipment collection */
exports.deleteEquipment = function (req, res) {

    db.Equipment.remove({_id: req.query.id}, function (err) {
        if (err) {
            res.status(500).send(err.message);
        } else {
            res.status(200).send("Delete done");
        }
    });
};

/* This method updates Equipment information to Equipment collection */
exports.updateEquipment = function (req, res) {
    
    var updateData = {
        Hevonen: req.body.Hevonen,
        Satula: req.body.Satula,
        Suitset: req.body.Suitset,
        Muut: req.body.Muut
    };
    
    db.Equipment.update({_id: req.body.id}, updateData, function (err) {
        
        if (err) {   
            res.status(500).send(err.message);
        } else {
            res.status(200).send("Updated");
        }
    });
};


/* This function gets all documents from Pasturage collection */
exports.getAllPasturages = function (req, res) {
    
    db.Pasturage.find(function (err, data) {
        
        if (err) {
            res.status(500).send("Error in database!!");
        } else {
            res.status(200).send(data);
        }
    });
};

/* This function saves new Pasturage information to Pasturage collection */
exports.saveNewPasturage = function (req, res) {
    
    var saveTemp = new db.Pasturage(req.body);
    
    //Save it to database
    saveTemp.save(function (err, ok) {
        
        if (err) {
            res.status(500).send("Error in database!!");
        } else {
            res.status(200).send("Save New Item OK!!");
        }
    });
};

/* This function deletes one field from Pasturage collection */
exports.deletePasturage = function (req, res) {

    db.Pasturage.remove({_id: req.query.id}, function (err) {
        if (err) {
            res.status(500).send(err.message);
        } else {
            res.status(200).send("Delete done");
        }
    });
};

/* This method updates Pasturage information to Pasturage collection */
exports.updatePasturage = function (req, res) {
    
    var updateData = {
        Hevonen: req.body.Hevonen,
        Omistaja: req.body.Omistaja,
        Yhteystiedot: req.body.Yhteystiedot,
        Tulopaiva: req.body.Tulopaiva,
        Lahtopaiva: req.body.Lahtopaiva,
        Laskutus: req.body.Laskutus,
        Huomio: req.body.Huomio
    };
    
    db.Pasturage.update({_id: req.body.id}, updateData, function (err) {
        
        if (err) {   
            res.status(500).send(err.message);
        } else {
            res.status(200).send("Updated");
        }
    });
};

/* This function gets all documents from PasturageGuest collection */
exports.getAllPasturageGuests = function (req, res) {
    
    db.PasturageGuest.find(function (err, data) {
        
        if (err) {
            res.status(500).send("Error in database!!");
        } else {
            res.status(200).send(data);
        }
    });
};

/* This function saves new PasturageGuest information to PasturageGuest collection */
exports.saveNewPasturageGuest = function (req, res) {
    
    var saveTemp = new db.PasturageGuest(req.body);
    
    //Save it to database
    saveTemp.save(function (err, ok) {
        
        if (err) {
            res.status(500).send("Error in database!!");
        } else {
            res.status(200).send("Save New Item OK!!");
        }
    });
};

/* This function deletes one field from PasturageGuest collection */
exports.deletePasturageGuest = function (req, res) {

    db.PasturageGuest.remove({_id: req.query.id}, function (err) {
        if (err) {
            res.status(500).send(err.message);
        } else {
            res.status(200).send("Delete done");
        }
    });
};

/* This method updates PasturageGuest information to PasturageGuest collection */
exports.updatePasturageGuest = function (req, res) {
    
    var updateData = {
        Hevonen: req.body.Hevonen,
        Omistaja: req.body.Omistaja,
        Yhteystiedot: req.body.Yhteystiedot,
        Tulopaiva: req.body.Tulopaiva,
        Lahtopaiva: req.body.Lahtopaiva,
        Laskutus: req.body.Laskutus,
        Huomio: req.body.Huomio
    };
    
    db.PasturageGuest.update({_id: req.body.id}, updateData, function (err) {
        
        if (err) {   
            res.status(500).send(err.message);
        } else {
            res.status(200).send("Updated");
        }
    });
};

/* This function gets all documents from Camp collection */
exports.getAllCamps = function (req, res) {
    db.Camp.find(function (err, data) {
        
        if (err) {
            res.status(500).send("Error in database!!");
        } else {
            res.status(200).send(data);
        }
    });
};

/* This function saves new Camp information to Camp collection */
exports.saveNewCamp = function (req, res) {
    
    var campTemp = new db.Camp(req.body);
    
    //Save it to database
    campTemp.save(function (err, ok) {
        if (err) {
            res.status(500).send("Error in database!!");
        } else {
            res.status(200).send("Save New Item OK!!");
        }
    });
};

/*This function deletes one Camp from Camp collection */
/*Function deletes also all campperson who are linked to that Camp */
exports.deleteCamp = function (req, res) {
    
    db.Camp.findOne({_id:req.query.id}, function(err,data){
        
        //Remove camppersons before removing camp
        db.Campperson.remove({_id:{$in:data.persons} }, function(err,removed){

            if (err) {
                res.status(500).send({messsage:err.message});
            } else {
                
                //Remove camp only if camppersons remove success
                db.Camp.remove({_id: req.query.id}, function (err, data) {

                    if (err) {
                        res.status(500).send({message:err.message});
                    } else {

                        if(err){;
                            res.status(500).send({messsage:err.message});
                        }else{
                            res.status(200).send({message:'Delete success'});
                        }
                    }
                });
            }
        });
    });
};

/* This method updates Camp information to Camp collection */
exports.updateCamp = function (req, res) {
    
    var updateData = {
        Leiri: req.body.Leiri,
        Alku: req.body.Alku,
        Loppu: req.body.Loppu,
        Huomio: req.body.Huomio,
        Osallistujat: req.body.Osallistujat
    };
    
    db.Camp.update({_id: req.body.id}, updateData, function (err) {
        if (err) {
            res.status(500).send(err.message);
        } else {
            res.status(200).send("Updated");
        }
    });
};

/* This function gets all ducuments from campPerson collection */
exports.getAllCampPersons = function (req, res) {
    db.Camp.findOne({Leiri:req.query.camppi}).populate('persons').exec(function(err,data){
        
        if (err) {
            res.status(500).send("Error in database!!");
        } else {
            res.status(200).send(data.persons);
        }
    });
};

/* This function saves new person information to Campperson collection */
exports.saveNewCampPerson = function (req, res) {
    
    var personTemp = new db.Campperson(req.body);
    
    //Save it to database
    personTemp.save(function (err, ok) {
        
        db.Camp.update({Leiri:req.body.Leiri},
                          {$push:{'persons':personTemp._id}},
                          function(err,model){
        
            if (err) {
                res.status(500).send("Error in database!!");
            } else {
                res.status(200).send("Save New Person OK!!");
            }
        });
    });
};

/* This function deletes one person from Campperson collection */
exports.deleteCampPerson = function (req, res) {
    db.Campperson.remove({_id: req.query.id}, function (err, data) {
        
        if (err) {
            res.status(500).send({message:err.message});
        } else {
            db.Camp.update({Leiri: req.query.Leiri}, {$pull: {'persons': req.query.id}}, function (err, data) {
                if(err){
                    res.status(500).send({messsage:err.message});
                } else {
                    res.status(200).send({message:'Delete success'});
                }
            });
        }
    });
};

/* This method updates person information to Campperson collection */
exports.updateCampPerson = function (req, res) {
    
    var updateData = {
        Nimi: req.body.Nimi,
        Osoite: req.body.Osoite,
        Puhelin: req.body.Puhelin,
        Sahkoposti: req.body.Sahkoposti,
        Yhteyshenkilo: req.body.Yhteyshenkilo,
        Varausmaksu: req.body.Varausmaksu,
        Loppulasku: req.body.Loppulasku,
        Huomio: req.body.Huomio
    };
    db.Campperson.update({_id: req.body.id}, updateData, function (err) {
        if (err) {
            res.status(500).json({message: err.message});
        } else {
            res.status(200).json({message: "Data updated"});
        }
    });
};


/* This function gets all documents from Feed collection */
exports.getAllFeeds = function (req, res) {
    db.Feed.find(function (err, data) {
        
        if (err) {
            res.status(500).send("Error in database!!");
        } else {
            res.status(200).send(data);
        }
    });
};

/* This function saves new Feed information to Feed collection */
exports.saveNewFeed = function (req, res) {
    
    var feedTemp = new db.Feed(req.body);
    
    //Save it to database
    feedTemp.save(function (err, ok) {
        if (err) {
            res.status(500).send("Error in database!!");
        } else {
            res.status(200).send("Save New Item OK!!");
        }
    });
};

/*
This function deletes one field from Feed collection
*/
exports.deleteFeed = function (req, res) {

    db.Feed.remove({_id: req.query.id}, function (err) {
        if (err) {
            res.status(500).send(err.message);
        } else {
            res.status(200).send("Delete done");
        }
    });
};

/* This method updates Health information to Feed collection */
exports.updateFeed = function (req, res) {
    
    var updateData = {
        Hevonen: req.body.Hevonen,
        Aamuruoka: req.body.Aamuruoka,
        Paivaruoka: req.body.Paivaruoka,
        Iltaruoka: req.body.Iltaruoka,
        Huomio: req.body.Huomio
    };
    
    db.Feed.update({_id: req.body.id}, updateData, function (err) {
        if (err) {
            res.status(500).send(err.message);
        } else {
            res.status(200).send("Updated");
        }
    });
};

/* This method add new user information to Person collection, do also password hash */
exports.registerUser = function (req, res) {

    var user = new db.Person();
    user.username = req.body.username;
    user.password = user.generateHash(req.body.password);
    user.Nimi = req.body.Nimi;
    user.Osoite = req.body.Osoite;
    user.Puhelin = req.body.Puhelin;
    user.Sahkoposti = req.body.Sahkoposti;
    user.Tyosuhde = req.body.Tyosuhde;
    user.userclass = req.body.userclass;
    
    user.save(function (err) {
        
        if (err) {
            res.status(500).send({status: err.message});
        } else {
            req.session.username = req.body.username;
            res.status(200).send({status: "Ok"});
        }
    });
};

/* This method check user information from Person collection and create token */
exports.loginUser = function (req, res) {
    var searchObject = {
        username: req.body.username
    };
    
    db.Person.findOne(searchObject, function (err, data) {
        
        if (err) {
            res.send(502, {status: err.message});
        } else {
            //=< 0 means wrong username or password
            if (data) {
                req.session.username = data.username;
                req.session.userclass = data.userclass;
                req.session.name = data.Nimi;
                //Create the token
                var token = jwt.sign(data, server.secret, {expiresIn: '8h'});
                res.send(200, {status: "Ok", secret: token, userclass: data.userclass, name: data.Nimi});
            } else {
                res.send(401, {status: "Wrong username or password"});
            }  
        }
    });
};

/* This method check user password information from Person collection */
exports.checkPass = function (req, res) {
    var searchObject = {
        username: req.session.username,
        password: req.body.password
    };
    
    db.Person.findOne(searchObject, function (err, data) {
        
        if (err) {
            res.send(502, {status: err.message});
        } else {
            //=< 0 means wrong username or password
            if (data) {
                res.send(200, {status: "Ok"});
            } else {
                res.send(401, {status: "Wrong username or password"});
            }  
        }
    });
};

/* This method check userclass information from Person collection */
exports.checkClass = function (req, res) {
    
    db.Person.find({username: req.session.username}).exec(function (err, data) {
        
        if (err) {
            res.status(500).send("Error in database!!");
        } else {
            res.status(200).send(data);
        }  

    });
};


/* This method updates user password information to person collection */
exports.changePass = function (req, res) {
    
    var user = new db.Person();
    user.password = user.generateHash(req.body.newpassword);

    var updateData = {
        password: user.password
    };
    
    db.Person.update({username: req.body.username}, updateData, function (err) {
        
        if (err) {
            res.status(500).json({message: err.message});
        } else {
            res.status(200).json({message: "Data updated"});
        }
    });
};

/* This method get user information from the Users collection */
exports.getUsersByUsername = function (req, res) {
    
    var usern = req.params.username.split("=")[1];

    db.Person.find({username: usern}).exec(function (err, data) {
             
         if (err) {
                res.status(500).send("Error in database!!");
         } else {
                res.status(200).send(data);
         }
    });
};


/* This function gets "Pehtoori" document from Text collection */
exports.readText = function (req, res) {

    db.Text.find({Text1: "Pehtoori"}, function (err, data) {

        if (err) {
            res.status(500).send("Error in database!!");
        } else {
            res.status(200).send(data);
        }
    });
};

/* This function write updated document to Text collection */
exports.writeText = function (req, res) {  

    var updateData = {
        Text2: req.body.text
    };
    
    db.Text.update({Text1: "Pehtoori"}, updateData, function (err) {
        if (err) {
            res.status(500).send(err.message);
        } else {
            res.status(200).send("Updated");
        }
    });
};
    
/* This function write Email settings to Text collection */
exports.writeEmailAddress = function (req, res) {  
    
    var updateData = {
        Text2: req.body.text2,
        Text3: req.body.text3
    };
    
    db.Text.update({Text1: "Email"}, updateData, function (err) {
        if (err) {
            res.status(500).send(err.message);
        } else {
            res.status(200).send("Updated");
        }
    });
};

/* This function gets Email settings from Text collection */
exports.readEmailAddress = function (req, res) {

    db.Text.find({Text1: "Email"}, function (err, data) {

        if (err) {
            res.status(500).send("Error in database!!");
        } else {
            res.status(200).send(data);
        }
    });
};

/* This function gets all documents from Test collection */
exports.getAllTests = function (req, res) {
        
    var temp = req.query.amount;
    if (temp === "" || temp === undefined || temp === null) {
        temp = 4;
    }

    db.Test.find(function (err, data) {

        if (err) {
            res.status(500).send("Error in database!!");
        } else {
            res.status(200).send(data);
        }
    }).limit(temp).sort({Aika: -1});

};

/* This function saves new Feed information to Test collection */
exports.saveNewTest = function (req, res) {
    
    var saveTemp = new db.Test(req.body);
    //Save it to database
    saveTemp.save(function (err, ok) {
        if (err) {
            res.status(500).send("Error in database!!");
        } else {
            res.status(200).send("Save New Item OK!!");
        }
    });
};

/*This function deletes one field from Test collection*/
exports.deleteTest = function (req, res) {
    
    db.Test.remove({_id: req.query.id}, function (err) {
        if (err) {
            res.status(500).send(err.message);
        } else {
            res.status(200).send("Delete done");
        }
    });
};

/***This function searches test database collection by name or by begin letters of name*/
exports.findTestItems = function(req,res){

    db.Test.find({ Test1: {'$regex': '^' + req.query.keyword, $options: 'i'}}, function(err, user) {
        if (err) {
            res.status(500).send("Error in database!!");
        } else {
            res.status(200).send(user);
        }
    });
}

/* This method updates test document information to test collection */
exports.updateTest = function (req, res) {
    
    var updateData = {
        Test1: req.body.Test1,
        Test2: req.body.Test2,
        Aika: req.body.Aika
    };
    
    db.Test.update({_id: req.body.id}, updateData, function (err) {
        if (err) {
            res.status(500).send(err.message);
        } else {
            res.status(200).send("Updated");
        }
    });
};

/*NodeEmailer*/
exports.sendEmail = function(req, res)  {
        
    //Create smtp connection
     var smtpTransport = nodemailer.createTransport("SMTP",{
        service: "Gmail",
        auth: {
            user: req.body.sender,
            pass: req.body.senderPass
        }
    });
     
    //Set email options
    var mailOptions={
        to : req.body.to,
        subject : req.body.subject,
        text : req.body.text
    }

    //send email
    smtpTransport.sendMail(mailOptions, function(error, response){
        if(error){
            res.status(500).send(error.message);
        }else{
            res.status(200).send(response.message);
        }
        
        //Close smtp connetion
        smtpTransport.close();
    });
};
