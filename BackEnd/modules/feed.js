"use strict";
var express = require("express");
var db = require('./queries');

var router = express.Router();

//Handle GET request for /Feeds.context
router.get('/', function (req, res) {
    db.getAllFeeds(req, res);
});

//Handle POST request for /Feeds.context
router.post('/', function (req, res) {
    
    db.saveNewFeed(req, res);
});

//Handle UPDATE request for /Feeds.context
router.put('/', function (req, res) {
    
    db.updateFeed(req, res);
});

//Handle DELETE request for /Feeds.context, id comes from request
router.delete('/', function (req, res) {
    
    db.deleteFeed(req, res);
});

module.exports = router;