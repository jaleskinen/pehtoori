"use strict";
var express = require("express");
var db = require('./queries');

var router = express.Router();

//Handle GET request for /camp.context
router.get('/', function (req, res) {
    db.getAllCamps(req, res);
});

//Handle POST request for /camp.context
router.post('/', function (req, res) {
    db.saveNewCamp(req, res);
});

//Handle UPDATE request for /camp.context
router.put('/', function (req, res) {
    db.updateCamp(req, res);
});

//Handle DELETE request for /camp.context
router.delete('/', function (req, res) {
    db.deleteCamp(req, res);
});

module.exports = router;