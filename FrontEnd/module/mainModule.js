//This is Pehtoori main module.
var main_module = angular.module('main_module', ['ngRoute', 'ngResource', 'ngAnimate', 'flash', 'angularUtils.directives.dirPagination']);

//This function will check if user is logged in or not. 
function loginRequired($q, $resource, $location, $http) {
    //Create a promise
    var deferred = $q.defer();
    $http.defaults.headers.common['x-access-token'] = sessionStorage['token'];
    $resource('/isLogged').query().$promise.then(function () {
        //Mark the promise to be solved (or resolved)
        deferred.resolve();
        return deferred;
        
    }, function () {
        
        //Mark promise to be failed
        deferred.reject();
        //Go back to root context
        $location.path('/');
        return deferred;
    });
}

//Create basic configuration for Angular
//Configuration includes routers for Pehtoori views.
main_module.config(function ($routeProvider) {
    
    $routeProvider.when('/', {
        templateUrl: 'login.html',
        controller: 'loginController'
    }).when('/main', {
        templateUrl: 'main.html',
        controller:  'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/admin', {
        templateUrl: 'admin.html',
        controller:  'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/addUser', {
        templateUrl: 'addUser.html',
        controller:  'loginController',
        resolve: {loginRequired: loginRequired}
    }).when('/changePassWord', {
        templateUrl: 'changePassWord.html',
        controller:  'loginController',
        resolve: {loginRequired: loginRequired}
    }).when('/horse', {
        templateUrl: 'horse.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/addHorse', {
        templateUrl: 'addHorse.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/modifyHorse', {
        templateUrl: 'modifyHorse.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/feedHorse', {
        templateUrl: 'feedHorse.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/addFeedHorse', {
        templateUrl: 'addFeedHorse.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/modifyFeedHorse', {
        templateUrl: 'modifyFeedHorse.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/healthHorse', {
        templateUrl: 'healthHorse.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/addHealthHorse', {
        templateUrl: 'addHealthHorse.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/modifyHealthHorse', {
        templateUrl: 'modifyHealthHorse.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/horseWelfare', {
        templateUrl: 'horseWelfare.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/addHorseWelfare', {
        templateUrl: 'addHorseWelfare.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/modifyHorseWelfare', {
        templateUrl: 'modifyHorseWelfare.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/horseEquipment', {
        templateUrl: 'horseEquipment.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/addHorseEquipment', {
        templateUrl: 'addHorseEquipment.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/modifyHorseEquipment', {
        templateUrl: 'modifyHorseEquipment.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/horsePasturage', {
        templateUrl: 'horsePasturage.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/addHorsePasturage', {
        templateUrl: 'addHorsePasturage.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/modifyHorsePasturage', {
        templateUrl: 'modifyHorsePasturage.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/horsePasturageGuest', {
        templateUrl: 'horsePasturageGuest.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/addHorsePasturageGuest', {
        templateUrl: 'addHorsePasturageGuest.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/modifyHorsePasturageGuest', {
        templateUrl: 'modifyHorsePasturageGuest.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/person', {
        templateUrl: 'person.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/addPerson', {
        templateUrl: 'addPerson.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/modifyPerson', {
        templateUrl: 'modifyPerson.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/contact', {
        templateUrl: 'contact.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/addContact', {
        templateUrl: 'addContact.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/modifyContact', {
        templateUrl: 'modifyContact.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/todo', {
        templateUrl: 'todo.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/addTodo', {
        templateUrl: 'addTodo.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/modifyTodo', {
        templateUrl: 'modifyTodo.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/stableCalendar', {
        templateUrl: 'stableCalendar.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/addStableCalendar', {
        templateUrl: 'addStableCalendar.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/modifyStableCalendar', {
        templateUrl: 'modifyStableCalendar.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/paddoc', {
        templateUrl: 'paddoc.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/addPaddoc', {
        templateUrl: 'addPaddoc.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/modifyPaddoc', {
        templateUrl: 'modifyPaddoc.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/campGoogle', {
        templateUrl: 'campGoogle.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/camp', {
        templateUrl: 'camp.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/addCamp', {
        templateUrl: 'addCamp.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/modifyCamp', {
        templateUrl: 'modifyCamp.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/campPerson', {
        templateUrl: 'campPerson.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/addCampPerson', {
        templateUrl: 'addCampPerson.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/modifyCampPerson', {
        templateUrl: 'modifyCampPerson.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/horseHour', {
        templateUrl: 'horseHour.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/addHorseHour', {
        templateUrl: 'addHorseHour.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/modifyHorseHour', {
        templateUrl: 'modifyHorseHour.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/personHour', {
        templateUrl: 'personHour.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/addPersonHour', {
        templateUrl: 'addPersonHour.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/modifyPersonHour', {
        templateUrl: 'modifyPersonHour.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/test', {
        templateUrl: 'test.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/addTest', {
        templateUrl: 'addTest.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    }).when('/modifyTest', {
        templateUrl: 'modifyTest.html',
        controller: 'mainController',
        resolve: {loginRequired: loginRequired}
    });
});