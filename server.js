var express = require('express');
var mongoose = require('mongoose');
var path = require('path');
var bodyParser = require("body-parser");
var jwt = require('jsonwebtoken');

//Mailer services
var nodemailer = require('nodemailer');
exports.nodemailer = nodemailer;
var smtpTransport = require('nodemailer-smtp-transport');

//This is used for creating a secret key value
var uuid = require('uuid');

//Create a secret for our web token and export it to other modules
var secret = uuid.v1();
exports.secret = secret;

//This is used to create a session object for client
var session = require('express-session');

//passport
//Initialize our passport
var passport = require('passport');
exports.passport = passport;
var pass = require('./BackEnd/modules/passport')(passport); 

var database = require('./BackEnd/modules/database');
var queries = require('./BackEnd/modules/queries');
var horse = require('./BackEnd/modules/horse');
var todo = require('./BackEnd/modules/todo');
var personhour = require('./BackEnd/modules/personhour');
var stablecalendar = require('./BackEnd/modules/stablecalendar');
var paddoc = require('./BackEnd/modules/paddoc');
var camp = require('./BackEnd/modules/camp');
var campperson = require('./BackEnd/modules/campperson');
var feed = require('./BackEnd/modules/feed');
var health = require('./BackEnd/modules/health');
var welfare = require('./BackEnd/modules/welfare');
var pasturage = require('./BackEnd/modules/pasturage');
var pasturageguest = require('./BackEnd/modules/pasturageguest');
var equipment = require('./BackEnd/modules/equipment');
var horsehour = require('./BackEnd/modules/horsehour');
var user = require('./BackEnd/modules/user');
var contact = require('./BackEnd/modules/contact');
var text = require('./BackEnd/modules/text');

//This is used only for testing purposes, to be removed from tthe final version!
var test = require('./BackEnd/modules/test');

var app = express();

//You need to define these two variables with these
//two environment variables to get you app work in openshift
var port = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080;
var ip   = process.env.IP   || process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0';

//=====================Middlewares========================//

app.use(session({
    secret: uuid.v1(),
    cookie: {maxAge: 28800000} //8h (=28800sec=28800000ms), if set to null => cookie are deleted after browser shut down
}));

app.use(passport.initialize());
app.use(passport.session());

//bodyParser urlencoded() middleware parses the json object from HTTP POST request
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());

app.use('/', express.static(path.join(__dirname, '/view')));
app.use('/FrontEnd/css', express.static(path.join(__dirname, '/FrontEnd/css')));
app.use('/FrontEnd/lib', express.static(path.join(__dirname, '/FrontEnd/lib')));
app.use('/FrontEnd/module', express.static(path.join(__dirname, '/FrontEnd/module')));
app.use('/FrontEnd/controllers', express.static(path.join(__dirname, '/FrontEnd/controllers')));
app.use('/FrontEnd/factories', express.static(path.join(__dirname, '/FrontEnd/factories')));
app.use('/FrontEnd/directives', express.static(path.join(__dirname, '/FrontEnd/directives')));

app.use('/horses', horse);
app.use('/todos', todo);
app.use('/personhours', personhour);
app.use('/stablecalendars', stablecalendar);
app.use('/paddocs', paddoc);
app.use('/camps', camp);
app.use('/camppersons', campperson);
app.use('/feeds', feed);
app.use('/healths', health);
app.use('/welfares', welfare);
app.use('/pasturages', pasturage);
app.use('/pasturageguests', pasturageguest);
app.use('/equipments', equipment);
app.use('/horsehours', horsehour);
app.use('/users', user);
app.use('/contacts', contact);
app.use('/texts', text);

//This is used only for testing purposes, to be removed from the final version!
app.use('/tests', test);

//=====================ROUTERS=================//
app.get('/logout', function (req, res) {
    req.session.destroy();
    res.redirect('/');
});

app.use(function (req, res, next) {
    //passport
    req.passport = passport;
    
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    //Check if there was a token
    if(token){
        //Verify that token is not 'guessed' by the client and it matches
        //the one we created in login phase
        jwt.verify(token, secret, function(err, decoded) {
            //If there was error verifying the token
            if(err) {
                
                return res.send(401);
            } else {
                
                req.decoded = decoded;
                next();
            }
        });
        
    } else {
                
        res.send(403);
    }
});

//passport
app.get('/authenticate',function(req,res){
    if(req.user){
        res.send({authenticated:true});
    }
    else{
        res.send({authenticated:false});
    }
});

//=====================ROUTERS=================//

//This router checks if client is logged in or not
app.get('/isLogged',function(req,res){
    //User is logged in if session contains kayttaja attribute
    if(req.session.username){
        res.status(200).send([{status:'Ok'}]);   
    }
    else{
        
       res.status(401).send([{status:'Unauthorized'}]);  
    }
});

//Listen the given port in given ip address
app.listen(port,ip);