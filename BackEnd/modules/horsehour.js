"use strict";
var express = require("express");
var db = require('./queries');

var router = express.Router();

//Handle GET request for /horsehours context
router.get('/', function (req, res) {
    
    db.getAllHorsehours(req, res);
});

//Handle POST request for /horsehours context
router.post('/', function (req, res) {
    
    db.saveNewHorsehour(req, res);
});

//Handle UPDATE request for /horsehours context
router.put('/', function (req, res) {
    
    db.updateHorsehour(req, res);
});

//Handle DELETE request for /horsehours context, id comes from request
router.delete('/', function (req, res) {
    
    db.deleteHorsehour(req, res);
});

//Handle search request for /horsehours context
router.get('/search', function (req, res) {
    db.findHorseHours(req, res);
});

module.exports = router;