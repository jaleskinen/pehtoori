"use strict";
var query = require('./queries');
var express = require("express");
var router = express.Router();

//Handle GET request for Contacts
router.get('/', function (req, res) {
    query.getAllContacts(req, res);
});

//Handle POST request for Contacts
router.post('/', function (req, res) {
    query.saveNewContact(req, res);
});

//Handle UPDATE request for Contacts
router.put('/', function (req, res) {
    query.updateContact(req, res);
});

//Handle DELETE request for Contacts
router.delete('/', function (req, res) {
    query.deleteContact(req, res);
});

module.exports = router;